#### RIPMIC

Computes a repeat-induced point mutation (RIP) index for a given set of sequences (genes and/or transposable elements (TEs) for instance, file hereafter called feature file) in fasta format, and along the genome (also provided in fasta format) in non-overlapping windows (length defined by the user). In addition to the RIP index, calculates the frequency of genomic features (such as genes or TEs) along the genome, provided in the feature fasta file.

RIP, first discovered in *Neurospora crassa* (Selker and Stevens, 1985; Selker 1997) (Ascomycota) usually affect dinucleotide sequences. However in *Microbotryum* fungi (Basidiomycota), RIP-like mutations are found affecting TTG trinucleotides instead (Hood *et al.*, 2005).

This script computes a RIP-like index as the ratio *t/n*, *t* (TargetRIPRatio column of the `RIPMIC.txt` output file) being the ratio of the RIP-affected sites (TTG + CAA trinucleotides, forward and reverse; PostRIPTarget column of the `RIPMIC.txt` output file) over the non RIP-affected sites (TCG + CGA, minus overlapping tetra-nucleotides TGCA; PreRIPTarget column of the `RIPMIC.txt` output file) for the RIP target sites, and *n* (NonTargetRIPRatio column of the `RIPMIC.txt` output file) being the same ratio for non-RIP target sites ([A,C,G]TG + CA[C,G,T] over [A,C,G]CG + CG[C,G,T] -  [A,C,G]CG[C,G,T]; PostRIPNonTarget and PreRIPNonTarget columns of the `RIPMIC.txt` output file, respectively), to control for sequence composition.

A RIP index greater than 1 is indicative of an excess of RIP-like mutation compared to random expectations in a given sequence or region (12th columns of the `RIPMIC.txt` output file). In the output file windowFeat.txt, the RIP index is normalized as *t/n* - 1, so that a normalized RIP index greater than 0 suggests that the genomic region is RIP-affected.


Example command line:

> `bash RIPMIC/RIPMIC.sh genome.fa features.fa workDir/ 1000`

`genome.fa` is the genome fasta file, with the first field of the header being the name of the contig (unique).

`feastures.fa` is the feature fasta file. The headers' fields are tab separated.
The first field is composed as following: `shortid|contig_name:start-stop|feature_id`, e.g., `MldSil1|MvSl-1064-A1-R4_A1:177418-178892|MvSl-1064-A1-R4_A1g00077`. The `feature_id` field is dispensable.
The second field is the location of the feature in the genome, e.g., `autosome`. This field is dispensable.
The third field is the feature type, i.e., `gene` or `TE`.
The forth field is the feature detailed type, e.g., `exon`, `ClassI:LTR:Copia`, `ClassII:TIR:CACTA`.

`workDir/` is the output directory. After computation, it contains two output file: `RIPMIC.txt`, containing the RIP idex values for each genomic feature, and `windowFeat.txt`, containing the frequency of genomic features and the RIP index per non-overlapping genomic window. The genomic features reported in `windowFeat.txt` are only: `gene`, `exon`, `TE`, `CI` (Class I retrotransposons), `LTR` (long-terminal repeat retrotransposons), `Copia` and `Ty3` (LTR superfamilies), `CII` (Class II transposons), `Helitron` and `TIR` (two transposons orders) and `unclassified` (unclassified TE copies).

`1000` is the length of the genomic window considered to produce `windowFeat.txt`, for instance 1000 bp = 1kb (careful, the coordinates in the `windowFeat.txt` output file are in kilobases (kb)).


This script was used to detect RIP-like footprint in Duhamel *et al.*, 2022 (BioRxiv).


### Requirements

To run this pipeline, you need to have installed:

- awk, which needs LC_NUMERIC to be set as "en_US.UTF-8" for floating numbers (can be set permanently by including `export LC_NUMERIC="en_US.UTF-8"` in your `.bashrc` file).




### References


Duhamel M, Hood ME, Rodriguez de la Vega RC, Giraud T. Dynamics of transposable element accumulation in the non-recombining regions of mating-type chromosomes in anther-smut fungi. bioRxIv; 2022. p. 2022.08.03.502670

Hood ME, Katawczik M, Giraud T. 2005. Repeat-induced point mutation and the population structure of transposable elements in Microbotryum violaceum. Genetics 170: 1081–1089.

Selker EU. 1997. Epigenetic phenomena in filamentous fungi: useful paradigms or repeat-induced confusion? Trends in Genetics 13: 296–301.

Selker EU, Stevens JN. 1985. DNA methylation at asymmetric sites is associated with numerous transition mutations. Proceedings of the National Academy of Sciences 82: 8114–8118.
