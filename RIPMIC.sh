#!/bin/bash

genomeFile=$1 # fasta genome
featFile=$2 # fasta feature file
outDir=$3 # output directory
window=$4 # window size for feature density statistics

echo "bash $0 $@"

mkdir -p $outDir

echo -e "#Contig\tstart\tend\tgene\texon\tTE\tCI\tLTR\tCopia\tTy3\tCII\tHelitron\tTIR\tunclassified\tRIPratioCorr" > ${outDir}/windowFeat.txt
echo -e "#Id\tshortid\tregion\tfeature\tdetail\tPreRIPTarget\tPostRIPTarget\tPreRIPNonTarget\tPostRIPNonTarget\tTargetRIPRatio\tNonTargetRIPRatio\tRIPRatioUncorr" > ${outDir}/RIPMIC.txt

genomeFileTMP=${genomeFile}.tmp

awk '{if($1~/^>/){if(id!=""){print id"\n"seq; seq=""}; id=$1} else {seq=seq""$0}}END{print id"\n"seq}' $genomeFile > $genomeFileTMP



awk -v outDir=$outDir -v window=$window 'BEGIN{OFS="\t"}{
	if(NR==FNR){
		if($0~/^>/){
			split($1,id,"|");
			split(id[2],c1,":"); contig=c1[1];
			split(c1[2],c2,"-"); start=c2[1]; end=c2[2];
			region=$2; category=$3; detail=$4;
			sp=id[1]; gsub(">","",sp);
			if(category=="gene"){ID=id[2]"|"id[3]} else {ID=id[2]}
			for(i=start;i<=end;i++){
				if(category=="gene"){GENE[contig" "i]=1}
				if(category=="TE"){TE[contig" "i]=1}
				if(detail=="exon"){EXON[contig" "i]=1}
				if(detail~/ClassI:/){CLASSI[contig" "i]=1}
				if(detail~/ClassII:/){CLASSII[contig" "i]=1}
				if(detail~/unclassified/){UNCLASS[contig" "i]=1}
				if(detail~/ClassI:LTR/){LTR[contig" "i]=1}
				if(detail~/ClassI:LTR:Copia/){COPIA[contig" "i]=1}
				if(detail~/ClassI:LTR:Ty3/){TY3[contig" "i]=1}
				if(detail~/ClassII:Helitron/){HELITRON[contig" "i]=1}
				if(detail~/ClassII:TIR/){TIR[contig" "i]=1}
			}
		} else {
		# Calculate RIPratio for each feature
		gsub("a","A",$0); gsub("t","T",$0); gsub("c","C",$0); gsub("g","G",$0);
		len=split($0,chars,"");
		for(c=1;c<len-1;c++){
                        trint[chars[c]""chars[c+1]""chars[c+2]]++;
                }
                for (tri in trint){
                        if(tri=="TTG" || tri=="CAA"){
                                tPoRT+=trint[tri];
                        } else if(tri=="TCG" || tri=="CGA"){
                                tPrRT+=trint[tri];
                        } else if(tri~/.TG/ || tri~/CA./){
                                tPoRnT+=trint[tri];
                        } else if(tri~/.CG/ || tri ~/CG./){
                                tPrRnT+=trint[tri];
                        }
                }
                #Now 4-mers for overlapping VCGB and TCGA
                for(c=1;c<len-2;c++){
                        fnt[chars[c]""chars[c+1]""chars[c+2]""chars[c+3]]++;
                }
                for (f in fnt){
                        if(f=="TCGA"){
                                FiR+=fnt[f];
                        } else if(f~/(A|C|G)CG(C|G|T)/){
                                FinR+=fnt[f];
                        }
                }
                # remove the count of VCGB 4-mer from the triPrRnT
                tPrRnT=tPrRnT-FinR;
                # remove the count of TCGA 4-mer from the triPrRT
                tPrRT=tPrRT-FiR;
		if(tPrRT==0){RT=0} else {RT=tPoRT/tPrRT};
		if(tPrRnT==0){RnT=0} else {RnT=tPoRnT/tPrRnT};
		if(RnT==0){tRR=0} else {tRR=RT/RnT};
                print ID,sp,region,category,detail,tPrRT,tPoRT,tPrRnT,tPoRnT,RT,RnT,tRR >> outDir"/RIPMIC.txt";
                tPoRT=tPrRT=tPoRnT=tPrRnT=FiR=FinR=0;
                delete dint; delete trint; delete fnt;
		}
	} else {
	        # Position is the first line of virtual table
	        if($0~/^>/){
			contig=$1; gsub(">","",contig);
	        } else {
			gsub("a","A",$0); gsub("t","T",$0); gsub("c","C",$0); gsub("g","G",$0);
	                # Build the array representing the virtual table :
	                # key is the position and value is the nucleotide at this position in this sequence
	                len=split($0,chars,"");
			cpt=1; # checkpoint for the window
			C=window; # initialize first window coordinate
			GE=T=E=CI=CII=U=L=CO=TY=HE=TI=0; # Initialize feature counts
			cptR=1; # checkpoint for RIPMIC
			# First create array for RIPMIC 4-mers for overlapping VCGB and TCGA
			for(c=1;c<len-2;c++){
				fnt[c]=chars[c]""chars[c+1]""chars[c+2]""chars[c+3];
				if(cptR==w || c==len-3){
					for (f=c-window;f<=c;f++){
						if(fnt[f]=="TCGA"){
							fiR[c]++;
						} else if(fnt[f]~/(A|C|G)CG(C|G|T)/){
							finR[c]++;
						}
					}
					delete fnt;
					cptR=0;
				}
				cptR++;
			}

			# Then calculate RIPratio and feature counts
        	        for(c=1;c<=len;c++){
				#RIPMIC first
				if(c<len-1){
					trint[c]=chars[c]""chars[c+1]""chars[c+2];
					if(cptR==window || c==len-1){
						for (tri=c-window;tri<=c;tri++){
							if(trint[tri]=="TTG" || trint[tri]=="CAA"){
								triPoRT++;
							} else if(trint[tri]=="TCG" || trint[tri]=="CGA"){
								triPrRT++;
							} else if(trint[tri]~/.TG/ || trint[tri]~/CA./){
								triPoRnT++;
							} else if(trint[tri]~/.CG/ || trint[tri]~/CG./){
								triPrRnT++;
							}
						}
						# remove the count of VCGB 4-mer from the triPrRnT
						triPrRnT=triPrRnT-finR[c];
						# remove the count of TCGA 4-mer from the triPrRT
						triPrRT=triPrRT-fiR[c];
						if(triPrRT==0){triRT=0} else {triRT=triPoRT/triPrRT};
						if(triPrRnT==0){triRnT=0} else {triRnT=triPoRnT/triPrRnT};
						if(triRnT==0){triRR=0} else {triRR=triRT/triRnT};
						cptR=0;
					}
				cptR++;
				}

				# then windowFeat
				if(GENE[contig" "c]=="1"){GE+=1}
				if(TE[contig" "c]=="1"){T+=1}
				if(EXON[contig" "c]=="1"){E+=1}
				if(CLASSI[contig" "c]=="1"){CI+=1}
				if(CLASSII[contig" "c]=="1"){CII+=1}
				if(UNCLASS[contig" "c]=="1"){U+=1}
				if(LTR[contig" "c]=="1"){L+=1}
				if(COPIA[contig" "c]=="1"){CO+=1}
				if(TY3[contig" "c]=="1"){TY+=1}
				if(HELITRON[contig" "c]=="1"){HE+=1}
				if(TIR[contig" "c]=="1"){TI+=1};

				if(cpt==window || cpt==len){
					# start and stop in kb
					print contig,(C-window)/1000,C/1000,GE/window,E/window,T/window,CI/window,L/window,CO/window,TY/window,CII/window,HE/window,TI/window,U/window,triRR-1 >> outDir"/windowFeat.txt"
					triPoRT=triPrRT=triPoRnT=triPrRnT=triRR=0;
					GE=T=E=CI=CII=U=L=CO=TY=HE=TI=0;
					C+=window;
					cpt=0
				};
        	                cpt++;

	                }
		}
		delete dint; delete trint; delete fnt; delete fiR; delete finR;
	}
}' $featFile $genomeFileTMP

rm $genomeFileTMP

